## Contexte

Implémentation de la formule TF-IDF. Le script va parcourir un corpus (ici, un dossier de fichiers texte), rechercher toutes occurences du mot recherché par l'utilisateur ainsi que ses dérivés.
Par exemple, si l'on a dans un fichier le mot "ciel" et le mot "cieux", et que l'utilisateur a recherché ciel, le script comptera deux occurences du terme recherché.


## Technique

Utilisation de la librairie suivante : https://github.com/ClaudeCoulombe/FrenchLefffLemmatizer
Commande PIP :  pip install git+https://github.com/ClaudeCoulombe/FrenchLefffLemmatizer.git

Pour lancer le programme, exécutez tfidf.py, renseignez un mot à chercher parmi les fichiers présents dans le dossier "documents".

Il ne gère pas les accents, et il y a un problème de lecture où le programme plante avec un fichier texte particulier mais nous n'avons pas trouvé pourquoi.
