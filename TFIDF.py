from french_lefff_lemmatizer.french_lefff_lemmatizer import FrenchLefffLemmatizer
import os, os.path
from math import log

def lemmatizer(files, searchWord):



    lemmatizer = FrenchLefffLemmatizer()

    if len(lemmatizer.lemmatize(searchWord, 'all')) == 0:
        counter = 0
        with open("./documents/" + str(files)) as file:
            data = str(file.read())
            counter += customCount(searchWord, data, lemmatizer)
    else:
        counter = 0
        for lemme in lemmatizer.lemmatize(searchWord, 'all'):
            with open("./documents/" + str(files)) as file:
                data = file.read()
                counter += customCount(lemme[0], data, lemmatizer)

    return counter

def customCount(stringToSearch, data, lemmatizer):
    count = 0
    for word in data.split():
        matchedOnce = False
        for lemme in lemmatizer.lemmatize(word, 'all'):
            if str(lemme[0]).lower() == stringToSearch.lower():
                if matchedOnce == False:
                    matchedOnce = True
                    count += 1
    return count

def tfIdf(searchWord):
    n = len(os.listdir('./documents'))
    df = 0
    wOccurence = 0
    wFile = ""
    w = 0
    for file in os.listdir('./documents'):
        occurences = lemmatizer(file, searchWord)
        if occurences != 0:
            df += 1
            newW = occurences * log(n/df, 10)
            print("occurences : " + str(occurences))
            print("w : " + str(newW))
            if newW > w:
                w = newW
                wOccurence = occurences
                wFile = str(file)
    print("df : " + str(df))
    print("n : " + str(n))

    return wFile, wOccurence






def start():
    searchWord = str(input("Enter the word to search : "))
    fileFound, occurences = tfIdf(searchWord)
    print("fileFound : " + fileFound)
    if not fileFound == "" or occurences < 0:
        print("We have found the most pertinent file with \"" + searchWord + "\" : " + fileFound + ", " + str(occurences) + " occurences")
    else:
        print("No match found in our files")




restart = True
while restart == True:
    start()
    choice = str(input("restart ? (y/n)"))
    if choice == "y":
        restart = True
    else:
        restart = False