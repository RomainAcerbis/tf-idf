Comment est-ce qu'on appelle un boomerang qui ne revient pas ?
Un chat mort.

2. Que dit un aveugle lorsqu'on lui donne du papier de verre ?
« C’est écrit serré. »

3. Pourquoi la petite fille tombe-t-elle de la balançoire?
Parce qu’elle n’a pas de bras.

4. Qu'est ce qui est pire qu'un bébé dans une poubelle ?
Un bébé dans deux poubelles.

5. Grâce à quoi peut-on enlever le chewing-gum dans les cheveux ?
Le cancer.

6. Quelle est la différence entre la sodomie et les épinards ?
Même avec du beurre, les enfants n’aiment pas.

7. Qu'est-ce qui est mieux que gagner une médaille d'or aux Jeux Paralympiques ?
Marcher.

8. Quelle partie du légume ne passe pas dans le mixer ?
La chaise roulante.

9. Comment reconnaît-on une lettre envoyée par un lépreux ?
La langue est collée au timbre.

10. Quel est le point commun entre une Ferrari et une enfant de 5 ans ?
Quand tu rentres dedans, tu déchires tout.

11. Pourquoi est-ce difficile de rompre avec une copine Japonaise ?
Tu dois larguer deux bombes avant qu’elle comprenne.

12. Que faire quand on trouve un épileptique en crise dans une baignoire ?
Ajouter de la lessive et y jeter son linge sale.

13. Comment sortir un bébé d'un mixer ?
Avec une paille.

14. Quelle est la différence entre un camp d'entrainement terroriste et un orphelinat ?
Je ne sais pas, je conduis juste le drone.

15. Pourquoi on ne peut pas la faire à un bébé avorté ?
Parce qu’il n’est pas né d’hier.

16. Qu'est-ce qui a deux pattes et qui saigne ?
Un demi-chien.

17. Maman, maman pourquoi tu gémis ?
Tais toi et lèche.

18. A quoi reconnaît on un chat écrasé ?
Il fait un centimètre de large.

19. Quelle est la différence entre un footballeur, un handballeur et un pédophile ?
Le footballeur marque du pied, le handballeur marque de la main, et le pédophile Marc Dutroux.

20. Peut-on prendre un bain quand on a la diarrhée ?
Oui si vous en avez assez.

21. Comment un parachutiste aveugle sait-il qu'il va toucher le sol ?
Il y a du mou dans la laisse du chien.

22. Quelle est la différence entre Paul Walker et un ordinateur ?
Quand mon ordinateur se plante, ça me soûle.

23. Qu'est ce qui est vert et qui pue ?
Un scout mort au fond d’un bois.

24. Quel est le légume officiel de l'Allemagne ?
Michael Schumacher.

25. Quel est le point commun entre un enfant africain et une fleur ?
Ils sont besoin d’eau pendant une semaine et après ils meurent.

26. Pourquoi les myopathes ne conduisent-ils jamais de voiture ?
Parce qu’ils n’atteignent jamais l’âge du permis.

27. Combien de tour fait un bébé dans un micro-onde avant d'exploser ?
Je sais pas, j’ai du mal à compter quand je me masturbe.

28. Qu'est-ce qui a 5 bras, 3 jambes et 2 pieds ?
La ligne d’arrivée au marathon de Boston.

29. Quelle est la différence entre Neil Armstrong et Michael Jackson ?
Neil Armstrong a marché sur la Lune alors que Michael Jackson a violé des gosses.